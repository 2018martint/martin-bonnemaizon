from tkinter import *
from tkinter.ttk import Combobox

#prise en main de GUI

# Window creation, root of the interface
window = Tk()

# Creation of a label (text line) that says Hello World ! and with as first parameter the previous window
label_field = Label(window,text="Hello World !")

# Display of the label

label_field.pack()


def affichage(u):
    return(u)

#SAISIE DE TEXTE
var_texte = StringVar()
ligne_texte = Entry(window, textvariable=var_texte, width=30)
ligne_texte.pack()

#BOUTON
bouton_quitter = Button(window, text="Quitter", command=affichage(var_texte.get()))
bouton_quitter.pack()
spin = Combobox(window, values = [1,2,3,4])
spin.pack()


#CASE A COCHER
var_case = IntVar()
case = Checkbutton(window, text="Ne plus poser cette question", variable=var_case)
case.pack()

var_case.get()
#Prend 0 si pas cochée et 1 si cochée
# Running of the Tkinter loop that ends when we close the windw
window.mainloop()
