import numpy as np
from game_of_life.generate_universe import *

def evolution_cell(universe, coord, etat):
    """

    :param universe: Univers sous forme d'array
    :param coord: coordonnés de la cellule à étudier
    :param etat:int, valeur de la cellule étudiée
    :return:
    """
    dim = np.shape(universe)
    c=0
    #Traitement du cas ou la cellule n'est pas au bord

    #On fait un diagnostic du voisinage
    liste_coord_voisins=[]
    for i in range(coord[0]-1, coord[0] + 2):
        for j in range(coord[1]-1, coord[1] + 2):
            if  (i,j) != (coord[0],coord[1]) and i< dim[0] and j < dim[1]:
                #On vérifie que ce voisin est dans la grille
                liste_coord_voisins.append((i,j))
                #Dans les autres cas on revient au départ (comme dans un tore !)
            if i == dim[0] and j < dim[1]:
                liste_coord_voisins.append((0,j))
            if j == dim[1] and i < dim[0]:
                liste_coord_voisins.append((i,0))
            if j == dim[1] and i == dim[0]:
                liste_coord_voisins.append((0,0))

#On supprime les coordonnés de la cellule étudiée

    for u in liste_coord_voisins:
        if universe[u]==1:
            c+=1
#Ici on a le nombre de cellules voisines vivantes
    if c==3 :
        return 1
    if c==2 :
        return etat
    return 0

def generation(universe):
    """
    :param universe: Univers (avec seed) sous forme de np.array
    :return: La génération suivante sous forme d'array
    """
    dim = np.shape(universe)
    resultat = np.zeros(np.shape(universe))
    for i in range(dim[0]):
        for j in range(dim[1]):
            resultat[(i,j)]=evolution_cell(universe, (i,j), universe[(i,j)])
            #On regarde pour chaque cellule son évolution avec evolution-cell et universe(non modifié!)
    return resultat



