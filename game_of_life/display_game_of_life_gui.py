from tkinter import *
from game_of_life.generate_universe import *
from game_of_life.game_life_simulate import *
from tkinter.ttk import Combobox
import time

"""Au moment de lancer l'animation, on aura un affichage avce matplotlib car nous n'avons pas réussi à finaliser l'affichage avce Tkinter
Si vous avez le temps de jeter un oeil à la fonction "lancer_simulation" (ligne 113) c'est la fonction que j'aurais voulu mettre en "command=lancer_simulation" dans le
bouton de lancement mais elle n'est pas fonctionnelle. J'ai quand même passé du temps dessus et je l'ai commentée."""

#Création de la fenêtre principale
gameoflife1 = Tk()

#Taille de l'unvers
Aff_taille1 = Label(gameoflife1, text ="Choix largeur univers :")
Aff_taille1.pack()
largeur = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=largeur, width=30)
ligne_texte.pack()

Aff_taille2 = Label(gameoflife1, text ="Choix hauteur univers :")
Aff_taille2.pack()
hauteur = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=hauteur, width=30)
ligne_texte.pack()

#Choix d'un type de graine
Aff_seed = Label(gameoflife1, text ="Choix d'une seed :")
Aff_seed.pack()
spinbox_graines = Combobox(gameoflife1, values = list(seeds.keys()))
spinbox_graines.pack()

#Choix de la position initiale de la graine
Aff_position1 = Label(gameoflife1, text ="Choix abscisse seed:")
Aff_position1.pack()
abscisse_seed = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=abscisse_seed, width=30)
ligne_texte.pack()

Aff_position2 = Label(gameoflife1, text ="Choix ordonnée seed :")
Aff_position2.pack()
ordonee_seed = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=ordonee_seed, width=30)
ligne_texte.pack()

#Choix de la coloration
Aff_cmap = Label(gameoflife1, text ="Choix d'une coloration:")
Aff_cmap.pack()
spinbox_couleurs = Combobox(gameoflife1, values = ["Greys", "Blues"])
spinbox_couleurs.pack()

#Choix du nombre d'itération
Aff_nb_iter = Label(gameoflife1, text ="Choix nombre de simulations :")
Aff_nb_iter.pack()
nombre = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=nombre, width=30)
ligne_texte.pack()

#Choix de la vitesse d'execution de l'animation
Aff_temps = Label(gameoflife1, text ="Temps d'affichage d'une génération :")
Aff_temps.pack()
temps_att = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=temps_att, width=30)
ligne_texte.pack()

#CheckButton pour sauvegarder ou non l'animation
var_case = IntVar()
case = Checkbutton(gameoflife1, text="Sauvegarder la simulation", variable=var_case)
case.pack()

def recuperer_saisies():
    """
    Fonction de récupération des données saisies et lancement de la simulation avce ces données
    """
    case_cochée=var_case.get()
    taille_univers=(int(hauteur.get()),int(largeur.get()))
    position_seed=(int(abscisse_seed.get()),int(ordonee_seed.get()))
    graine_choisie=spinbox_graines.get()
    vitesse=int(temps_att.get())
    nb_iter=int(nombre.get())
    couleur=spinbox_couleurs.get()
    simulate_whole_game(taille_univers,seeds[graine_choisie],position_seed,couleur,nb_iter,vitesse,case_cochée)
    return [taille_univers,position_seed,graine_choisie,vitesse,nb_iter,couleur,case_cochée]

#Boutton pour quitter
bouton_quitter = Button(gameoflife1, text="Quitter", command=gameoflife1.quit)
bouton_quitter.pack()

gameoflife = Toplevel(gameoflife1)
gameoflife.grid()
#creation des fenetres
graphical_grid = Canvas(gameoflife,bg='white',height=750,width=750)
graphical_grid.pack()
#creation et placement du canvas

def calcul_taille_pixel(univers, max):
    """retourne la taille que doit avoir une cellule en pixel par rapport à la taille de l'univers et du canvas"""
    nbr_case_x=univers.shape[0]
    nbr_case_y=univers.shape[1]
    graphical_grid["height"]=max
    graphical_grid["width"]=max
    #on initialise la taille du canvas à un carre de max*max pixels
    if nbr_case_x>nbr_case_y :
        graphical_grid["width"]=nbr_case_y*max/nbr_case_x
        #on redimensionne le canvas
        return max/nbr_case_x
    else :
        graphical_grid["height"]=nbr_case_x*max/nbr_case_y
        #on redimensionne le canvas
        return max/nbr_case_y


def lancer_simulation():
    """
    :return: Lance la simulation et l'affiche dans une fenetre TopLevel
    """
    #on récupère toutes les données saisies
    taille_univers=(int(hauteur.get()),int(largeur.get()))
    position_seed=(int(abscisse_seed.get()),int(ordonee_seed.get()))
    graine_choisie=spinbox_graines.get()
    vitesse=int(temps_att.get())
    nb_iter=int(nombre.get())
    couleur=spinbox_couleurs.get()
    liste=[taille_univers,position_seed,graine_choisie,vitesse,nb_iter,couleur]

    nb_iter=liste[4]

    #Pour modifier le temps d'attente (penser à remettre temps_attente=liste[3] à la fin)
    temps_attente=0.3
    univers_avec_graine=add_seed_to_universe(seeds[liste[2]],generate_universe(liste[0]),liste[1][0],liste[1][1])

    #Pour modifier a taille d'un pixel
    taille_pixel=75

    for iter in range(nb_iter):
        #boucle du nombre d'itérations
        grille=univers_avec_graine
        taille_univers=liste[1]
        for x in range(taille_univers[0]):
            for y in range(taille_univers[1]):
                #on parcourt l'univers
                if grille[(x,y)]==1:
                    graphical_grid.create_rectangle(y*taille_pixel,x*taille_pixel,(y+1)*taille_pixel,(x+1)*taille_pixel,fill='black')
                else:
                    graphical_grid.create_rectangle(y*taille_pixel,x*taille_pixel,(y+1)*taille_pixel,(x+1)*taille_pixel,fill='white')
        grille=generation(grille)
        #on a la nouvelle grille
        time.sleep(temps_attente)
        #temps d'attente et update de l'univers
        graphical_grid.update()

#Boutton de lancement de la simulation
bouton_simulation = Button(gameoflife1, text="Lancer la simulation", command=recuperer_saisies)
bouton_simulation.pack()

gameoflife1.mainloop()

