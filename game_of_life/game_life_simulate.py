from matplotlib import pyplot as plt
from game_of_life.evolution_universe import *
from game_of_life.generate_universe import *
import matplotlib.animation as animation

def game_life_simulate(nb_iter,universe_with_seed):
    universe=universe_with_seed
    for iter in range(nb_iter):
        universe=generation(universe)
    return universe

def simu(nb_iter,universe_with_seed,interva,cmap_choice):
    fig = plt.figure()
    im = plt.imshow(universe_with_seed,cmap=cmap_choice, animated=True)
    def updatefig(*args):
        universe=im.get_array()
        im.set_array(generation(universe))
        return im,
    ani = animation.FuncAnimation(fig, updatefig, frames=nb_iter, interval=interva, blit=True)
    plt.show()

def simulate_whole_game(size,seed,seed_position,cmap_choice,n_generations,interval,save):
    """
    :param tuple: (int, int) universe_size: dimensions of the universe
    :param seed: (list of lists, np.ndarray) initial starting array
    :param seed_position: (tuple (int, int)) coordinates where the top-left corner of the seed array should be pinned
    :param cmap: (str) the matplotlib cmap that should be used
    :param n_generations: (int) number of universe iterations, defaults to 30
    :param interval: (int )time interval between updates (milliseconds), defaults to 300ms
    :param save: (bool) whether the animation should be saved, defaults to False
    """
    universe=generate_universe(size)
    (xstart,ystart)=seed_position
    universe=add_seed_to_universe(seed,universe,xstart,ystart)
    simu(n_generations,universe,interval,cmap_choice)
    # if save:
    #     animation.save

#simulate_whole_game((10,10),seeds["r_pentomino"],(1,1),'Greys',50,300,True)
