from game_of_life.game_life_simulate import *
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("size1", help = "Choisissez le nb de ligne de l'univers", type = int)
parser.add_argument("size2", help = "Choisissez le nb de colonne de l'univers", type = int)
parser.add_argument("seed", help = "Choix de l'amorce", type = str)
parser.add_argument("seed_position1", help = "Choix de position de ligne de l'amorce", type = int)
parser.add_argument("seed_position2", help = "Choix de position de colonne de l'amorce", type = int)
parser.add_argument("cmap", help = "Choix des couleurs", type = str)
parser.add_argument("n_generations", help = "Choix du nombre de générations", type = int)
parser.add_argument("interval", help = "Choix du temps d'affichage en ms d'une génération", type = int)
parser.add_argument("save", help = "Sauvegarder ou non ?", type = bool)

args = parser.parse_args()

#print(simulate_whole_game((args.size1,args.size2),seeds[args.seed],(args.seed_position1,args.seed_position2),args.cmap,args.n_generations,args.interval,args.save))
