import numpy as np
import random as rd

a=[
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,1,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,1,1,0,0,1,0,1,1,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0,1,0,1,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,1,0,1,0,1,1,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,1,1,0,0],
        [1,1,1,1,0,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,1,0,1,1,1,0,0],
        [1,0,0,0,1,1,0,1,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,1,1,0],
        [1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,1,0,0,1,1,0,1,0,0,1,0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,1,1,1],
    ]

y=a+a[::-1]


a=[
    [0 for i in range(36)],
    [0 for i in range(25)]+[1]+[0 for i in range(10)],
    [0 for i in range(22)]+[1,1,1,1]+[0,0,0,0]+[1]+[0 for i in range(5)],
    [0 for i in range(13)]+[1]+[0 for i in range(7)]+[1,1,1,1]+[0,0,0,0,0]+[1]+[0 for i in range(5)],
    [0 for i in range(12)]+[1,0,1]+[0 for i in range(6)]+[1,0,0,1]+[0 for i in range(9)]+[1,1],
    [0 for i in range(11)]+[1]+[0,0,0,1,1,0,0,0,0]+[1,1,1,1]+[0 for i in range(9)]+[1,1],
    [1,1]+[0 for i in range(9)]+[1,0,0,0,1,1]+[0 for i in range(5)]+[1,1,1,1]+[0 for i in range(10)],
    [1,1]+[0 for i in range(9)]+[1,0,0,0,1,1]+[0 for i in range(5)]+[0,0,0,1]+[0 for i in range(10)],
    [0 for i in range(12)]+[1,0,1]+[0 for i in range(21)],
    [0 for i in range(13)]+[1]+[0 for i in range(22)]
]

o=np.array(a)


seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
    "oscillateur_4blocs" : [
        [1,1,0,0,1,1,1,0,0,1,1],
        [1,0,0,0,0,0,0,0,0,0,1],
        [0,0,1,0,1,0,1,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,0,0],
        [1,0,1,0,0,0,0,0,1,0,1],
        [1,0,0,0,0,0,0,0,0,0,1],
        [1,0,1,0,0,0,0,0,1,0,1],
        [0,0,0,0,0,0,0,0,0,0,0],
        [0,0,1,0,1,0,1,0,1,0,0],
        [1,0,0,0,0,0,0,0,0,0,1],
        [1,1,0,0,1,1,1,0,0,1,1]
    ],
    "Horloge" : [
        [0,0,0,0,0,0,1,1,0,0,0,0],
        [0,0,0,0,0,0,1,1,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,1,1,0,0,0,0],
        [1,1,0,1,0,0,0,0,1,0,0,0],
        [1,1,0,1,0,1,1,0,1,0,0,0],
        [0,0,0,1,0,0,0,1,1,0,1,1],
        [0,0,0,1,0,0,0,0,1,0,1,1],
        [0,0,0,0,1,1,1,1,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,0,0,0,0,0,0],
        [0,0,0,0,1,1,0,0,0,0,0,0]
    ],
    "Mastodonte": y,
    "Glider":[[0,1,0],
              [0,0,1],
              [1,1,1]],
    "Canon": o
}


def generate_universe(size):
    """
    :param size: tuple (lignes,colonnes)
    :return: Un array de zéros de taille(size[0], size[1])
    """
    universe = np.zeros(size)
    return universe


def catalogue_seed(n,taille):
    """
    :param n: nombre d'amorces aléatoires différentes créées dans le catalogue
    :param taille: taille des amorces
    :return: Un dictionnaire (clé : nom de la seed (string), Valeur : Un array la représentant)
    """
    dict={}
    liste_nom=["seed_"+str(k) for k in range(n)]
    c=0
    #compteur pour les noms de seed
    while len(dict)<n:
        #création d'une matrice
        universe = generate_universe(taille)
        for k in range(taille[0]):
            for i in range(taille[1]):
                universe[k,i]=rd.randint(0,1)
        #... modifiée aléatoirement
        if universe not in dict.values():
            #On regarde si la seed a déjà été mise dans le dictionnaire
            dict[liste_nom[c]]=universe
            c+=1
    return dict


def placement_aleatoire(seed, size):
    """
    Attention : marche pour des seed au moins de tailles (2,2) mais pas grave car une seule cellule vivante dans un univers mort meurt à la prochaine iteration
    :param seed: une graine à implanter aléatoirement dans l'univers
    :param size: tuple (lignes,colonnes)
    :return: renvoie l'univers avec la seed placée aléatoirement
    """
    universe = generate_universe(size)
    taille_seed=np.shape(seed)
    #Choix aléatoire d'une case de départ dans l'écriture de l'amorce
    indice_ligne = rd.randint(0,size[0]-taille_seed[0])
    indice_colonne = rd.randint(0,size[1]-taille_seed[1])
    #Ecriture de l'amorce
    universe[indice_ligne:indice_ligne+taille_seed[0],indice_colonne:indice_colonne+taille_seed[1]]=seed
    return universe


#Fonction créée pour le test (différente de la notre qui place aléatoirement l'amorce)

def add_seed_to_universe(seed, universe, xstart, ystart):
    """
    :param seed: graine sous forme de liste de liste
    :param universe: univers sous forme de np.array
    :param xstart: abscisse de la position du coin en haut à gauche de la graine
    :param ystart: ordonnée de la position du coin en haut à gauche de la graine
    :return:
    """
    #Choix déterministe d'une case de départ dans l'écriture de l'amorce
    taille_seed = np.shape(seed)
    universe[xstart:xstart+taille_seed[0],ystart:ystart+taille_seed[1]] = seed
    #Ecriture de l'amorce
    return universe




