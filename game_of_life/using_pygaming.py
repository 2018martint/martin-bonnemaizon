import pygame
from pygame import *

from game_of_life.evolution_universe import *

from tkinter import *

from game_of_life.generate_universe import *

from game_of_life.game_life_simulate import *
from tkinter.ttk import Combobox

#constants
white=(255,255,255)
black=(0,0,0)

colors={"Black":(0,0,0),"White":(255,255,255),"Red":(255,0,0),"Green":(0,255,0),"Blue":(0,0,255),"Purple":(168,11,206)}

#Création de la fenêtre principale
gameoflife1 = Tk()

#Taille de l'unvers
Aff_taille1 = Label(gameoflife1, text ="Choix largeur univers :")
Aff_taille1.pack()
largeur = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=largeur, width=30)
ligne_texte.pack()

Aff_taille2 = Label(gameoflife1, text ="Choix hauteur univers :")
Aff_taille2.pack()
hauteur = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=hauteur, width=30)
ligne_texte.pack()

#Choix d'un type de graine
Aff_seed = Label(gameoflife1, text ="Choix d'une seed :")
Aff_seed.pack()
spinbox_graines = Combobox(gameoflife1, values = list(seeds.keys()))
spinbox_graines.pack()

#Choix de la position initiale de la graine
Aff_position1 = Label(gameoflife1, text ="Choix abscisse seed:")
Aff_position1.pack()
abscisse_seed = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=abscisse_seed, width=30)
ligne_texte.pack()

Aff_position2 = Label(gameoflife1, text ="Choix ordonnée seed :")
Aff_position2.pack()
ordonee_seed = StringVar()
ligne_texte = Entry(gameoflife1, textvariable=ordonee_seed, width=30)
ligne_texte.pack()

#Choix d'une coloration
Aff_couleur_vivante = Label(gameoflife1, text ="Choix d'une coloration cellules vivantes")
Aff_couleur_vivante.pack()
couleur_vivante = Combobox(gameoflife1, values = list(colors.keys()))
couleur_vivante.pack()
Aff_couleur_morte = Label(gameoflife1, text ="Choix d'une coloration cellules mortes")
Aff_couleur_morte.pack()
couleur_morte = Combobox(gameoflife1, values = list(colors.keys()))
couleur_morte.pack()


#main game function
def play():
        #initialization
        universe=add_seed_to_universe(seeds[spinbox_graines.get()],generate_universe((int(hauteur.get()),int(largeur.get()))),int(abscisse_seed.get()),int(ordonee_seed.get()))
        pygame.init()
        scrn = pygame.display.set_mode((10*np.shape(universe)[0], 10*np.shape(universe)[1]))
        mainsrf = pygame.Surface((10*np.shape(universe)[0], 10*np.shape(universe)[1]))
        mainsrf.fill(white)
        couleur_choisie_vivante=couleur_vivante.get()
        couleur_choisie_morte=couleur_morte.get()

        #game cycle
        while 1:
                #tracking quitting
                for event in pygame.event.get():
                        if event.type == QUIT:
                                pygame.quit()
                                sys.exit()
                #drawing
                for y in range(np.shape(universe)[1]):
                        for x in range(np.shape(universe)[0]):
                                if universe[x][y]==1:
                                        pygame.draw.rect(mainsrf, colors[couleur_choisie_vivante], (x*10, y*10, 10, 10))
                                else:
                                        pygame.draw.rect(mainsrf, colors[couleur_choisie_morte], (x*10, y*10, 10, 10))

                universe=generation(universe)
                scrn.blit(mainsrf, (0, 0))
                pygame.display.update()


#Boutton de lancement de la simulation
bouton_simulation = Button(gameoflife1, text="Lancer la simulation", command=play)
bouton_simulation.pack()

#Boutton pour quitter
bouton_quitter = Button(gameoflife1, text="Quitter", command=gameoflife1.quit)
bouton_quitter.pack()

gameoflife1.mainloop()

