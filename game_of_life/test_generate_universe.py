from game_of_life.generate_universe import *
from pytest import *
from game_of_life.evolution_universe import*
from game_of_life.game_life_simulate import *
from game_of_life.plot_universe import *

def test_generate_universe():
    assert np.array(generate_universe((4,4)) == [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]).all()

def test_add_seed_to_universe():
    seed = seeds["r_pentomino"]
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,xstart=1, ystart=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()

def test_placement_aleatoire():
    #il est difficile de tester une fonction aléatoire
    #on peut tester si la taille de l'univers est bonne
    #et si au moins 3 cellules sont vivantes
    U=placement_aleatoire(seeds["r_pentomino"],(10,10))
    (a,b)=np.shape(placement_aleatoire(seeds["r_pentomino"],(10,10)))
    assert (a,b)==(10,10)
    c=0
    for i in range(a):
        for j in range(b):
            if U[i,j]==1:
                c+=1
    assert c>=3


def test_evolution_cell():
    U=np.array([[1,1,1],[0,0,0],[0,0,0]])
    Unew=evolution_cell(U,(1,2),0)
    assert Unew == 1

def test_generation():
    U=np.array([[1,1,1],[0,0,0],[0,0,0]])
    Unew = np.array([[1,1,1],[1,1,1],[1,1,1]])
    assert np.array(generation(U) == Unew).all()


def test_game_life_simulate():
    U=np.array([[1,1,1],[0,0,0],[0,0,0]])
    assert np.array(game_life_simulate(1,U)==np.ones((3,3))).all()
